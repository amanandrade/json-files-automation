import yaml
import json
import re

# yamlfile = "C:\\Users\\amanda.m.andrade\\Documents\\area-de-trabalho\\Jenkins\\jenkins-jobs\\teste.yaml"
yamlfile = https://gitlab.com/amanandrade/json-files-automation/-/raw/main/openapisample.yaml

class Loadfile:
    def __init__ (self, yamlfile):
        self.yamlfile = yamlfile

    with open(yamlfile, 'r') as stream:
        try:
            json_object = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
        
    pathslist = json_object["paths"].values()
    index = len(pathslist)
    for i in range(0,index):
        routes = list(pathslist)[i]
        for i in routes.values():
            routesname = re.sub("[/]","-", i.get("operationId"))
            if (routesname.find("{") != -1):
                routesname = re.sub("[{].*$","id", routesname)
            files = open(routesname + ".json", "a")
