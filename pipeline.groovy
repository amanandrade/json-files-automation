
pipeline {
    // def server = Artifactory.server "SERVER_ID"
    agent {label 'maquinaamanda'}

    environment{
        gitlabConfigRepoUrl = "https://gitlab.com/amanandrade/json-files-automation.git"
        pythonScript = "https://gitlab.com/amanandrade/json-files-automation/-/raw/main/parsefile.py"
    }

    stages{
        stage('SCM'){
            steps{
                script{
                    echo 'Feating on source control'
                    checkout([$class: 'GitSCM', branches: [[name: '*/main']], doGenerateSubmoduleConfiguration: false, 
                    submoduleCfg: [], userRemoteConfigs: [[url: "${gitlabConfigRepoUrl}"]]])
                }
            }
        }

        stage('Build'){
            steps{
                script{
                    echo 'Building...' 
                    // load [[url:"${pythonScript}"]]
                    // sh("echo Loaded file ${pythonScript} successfuly.")

                    def cmdArray = ["python", "-c", [[url:"${pythonScript}"]]]
                    def cmd = cmdArray.execute()
                    // echo cmd
                    
                    
                    // powershell label: '', returnStdout: true, script: '''
                    //     Write-Host  git pull origin release/${env:version-build}
                    //     cd c:/build/repositorios/erp-wms
                    //     git pull origin release/${env:version-build}.xxx                      
                    // '''


                    // sh "mkdir -p AWS-backends/backend-DES"
                    // writeFile file: "AWS-backends/backend-DES/get-id-microsservico.json" , text: "{}"
                    // def cmdArray = ["python", "${pythonScript}", path]
                    // cmdArray.execute()
                }
            }
        }

        // stage('Test'){
        //     echo 'Testing...'
        // }

        stage('Deploy'){
            steps{
                script{
                    echo 'Deploying...'
                    // archiveArtifacts artifacts: "AWS-backends/backend-DES/*.json"
                }
            }
        }
    }
}